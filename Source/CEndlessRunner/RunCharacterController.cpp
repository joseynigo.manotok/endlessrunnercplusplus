// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "Components/InputComponent.h"
#include "RunCharacter.h"
#include "GameFramework/PlayerController.h"

ARunCharacterController::ARunCharacterController()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();
	PlayerCharacter = Cast<ARunCharacter>(GetCharacter());
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	check(InputComponent);

	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::CallMoveRight);
}

void ARunCharacterController::CallMoveRight(float scale)
{
	PlayerCharacter->MoveRight(scale);
}

void ARunCharacterController::Disable()
{
	DisableInput(Cast<ARunCharacterController>(this));
}