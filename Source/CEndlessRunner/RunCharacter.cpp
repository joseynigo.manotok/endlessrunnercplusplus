// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacter.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/InputComponent.h"
#include "Obstacle.h"
#include "RunGameMode.h"
#include "Components/SceneComponent.h"
#include "RunCharacterController.h"
#include "Components/SkeletalMeshComponent.h"

// Sets default values
ARunCharacter::ARunCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	IsDead = false;

	bUseControllerRotationPitch = true;
	bUseControllerRotationYaw = true;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	Skeleton = FindComponentByClass<USkeletalMeshComponent>();

	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	SpringArm->SetupAttachment(RootComponent);
	SpringArm->TargetArmLength = 500.0f;
	SpringArm->bUsePawnControlRotation = true;

	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	Camera->SetupAttachment(RootComponent);
	Camera->SetupAttachment(SpringArm);
	Camera->bUsePawnControlRotation = true;

 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARunCharacter::BeginPlay()
{
	Super::BeginPlay();
	Coin = 0;
}

void ARunCharacter::Die()
{
	UE_LOG(LogTemp, Warning, TEXT("FunctionCalled"));
	if (IsDead == true)
	{
		Skeleton->SetVisibility(false);
		Cast<ARunCharacterController>(GetController())->Disable();
		UE_LOG(LogTemp, Warning, TEXT("FunctionExecuted"));
		OnDeath.Broadcast(this);
	}

}

void ARunCharacter::MoveForward(float scale)
{
	if ((Controller != NULL) && (scale != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, scale);
	}
}

void ARunCharacter::MoveRight(float scale)
{
	if ((Controller != NULL) && (scale != 0.0f))
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, scale);
	}
}

void ARunCharacter::AddCoin()
{
	Coin++;
}

// Called every frame
void ARunCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!IsDead)
	{
		MoveForward(1.0f);
	}
}

// Called to bind functionality to input
void ARunCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

