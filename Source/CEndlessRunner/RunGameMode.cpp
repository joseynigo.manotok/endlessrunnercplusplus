// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "RunTile.h"
#include "RunCharacter.h"
#include "Components/ArrowComponent.h"
#include "Kismet/GameplayStatics.h"

ARunGameMode::ARunGameMode()
{

}

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();

	for (size_t i = 0; i < 3; i++)
	{
		Tile = GetWorld()->SpawnActor<ARunTile>(TileToSpawn, AttachPointTransform);
		AttachPointTransform = Tile->AttachPoint->GetComponentTransform();
	}

	SpawnTile(Tile);
}

void ARunGameMode::SpawnTile(ARunTile* CurrentTile) //remove param
{
	Tile = GetWorld()->SpawnActor<ARunTile>(TileToSpawn, AttachPointTransform);
	AttachPointTransform = Tile->AttachPoint->GetComponentTransform();

	for (int i = 0; i < 3; i++)
	{
		TempTile = GetWorld()->SpawnActor<ARunTile>(TileToSpawn, AttachPointTransform);
		AttachPointTransform = TempTile->AttachPoint->GetComponentTransform();
	}

	Tile->OnCharacterOverlap.AddDynamic(this, &ARunGameMode::SpawnTile);

	UE_LOG(LogTemp, Warning, TEXT("EventHeard"));
}
