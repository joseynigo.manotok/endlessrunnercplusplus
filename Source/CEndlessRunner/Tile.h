// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

UCLASS()
class CENDLESSRUNNER_API ATile : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ATile();


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UArrowComponent* AttachPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UBoxComponent* ExitTrigger;

	UFUNCTION()
		void ExitOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void DestroyActorOnTimer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	FTimerHandle TileTimer;

	UPROPERTY(EditAnywhere)
		int32 Timer = 10;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
