// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

/**
 * 
 */
UCLASS()
class CENDLESSRUNNER_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ARunGameMode();

	UPROPERTY(EditAnywhere)
		FTransform AttachPointTransform;

	UPROPERTY(EditAnywhere)
		class ARunTile* Tile;

	UPROPERTY(EditAnywhere)
		class ARunTile* TempTile;

	UPROPERTY(EditAnywhere)
		class UArrowComponent* AttachPoint;

protected:

	virtual void BeginPlay() override;

	UFUNCTION()
		void SpawnTile(class ARunTile* CurrentTile);

	UFUNCTION(BlueprintImplementableEvent)
		void RestartLevel(ARunCharacter* PlayerCharacter);

private:
	UPROPERTY(EditAnywhere)
		TSubclassOf<class ARunTile> TileToSpawn;
};
