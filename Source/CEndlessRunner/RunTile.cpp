// Fill out your copyright notice in the Description page of Project Settings.


#include "RunTile.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "RunCharacter.h"
#include "TimerManager.h"
#include "Obstacle.h"
#include "PickUP.h"
#include "Components/ChildActorComponent.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ARunTile::ARunTile()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("SceneComponent");
	SetRootComponent(SceneComponent);

	AttachPoint = CreateDefaultSubobject<UArrowComponent>("AttachPoint");
	AttachPoint->SetupAttachment(SceneComponent);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>("ExitTrigger");
	ExitTrigger->SetupAttachment(SceneComponent);

	ObstacleSpawnPoint = CreateDefaultSubobject<UBoxComponent>("ObstacleSpawnPoint");
	ObstacleSpawnPoint->SetupAttachment(SceneComponent);
}

// Called when the game starts or when spawned
void ARunTile::BeginPlay()
{
	Super::BeginPlay();
	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ARunTile::ExitOverlap);
	SpawnObstacle();
	//UE_LOG(LogTemp, Warning, TEXT("Test"));
}

void ARunTile::ExitOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* player = Cast<ARunCharacter>(OtherActor))
	{
		GetWorld()->GetTimerManager().SetTimer(TileTimer, this, &ARunTile::DestroyActorOnTimer, 2.0f, true);
		UE_LOG(LogTemp, Warning, TEXT("PlayerHitWall"));
		OnCharacterOverlap.Broadcast(this);
	}
}

void ARunTile::DestroyActorOnTimer()
{
	UE_LOG(LogTemp, Warning, TEXT("BOOM"));
	GetWorld()->GetTimerManager().ClearTimer(TileTimer);
	if (TileObstacle != NULL)
	{
		TileObstacle->Destroy();
	}

	if (TilePickUp != NULL)
	{
		TilePickUp->Destroy();
	}

	Destroy();
}

void ARunTile::SpawnObstacle()
{
	ObstacleLocation = UKismetMathLibrary::RandomPointInBoundingBox(ObstacleSpawnPoint->GetComponentLocation(), ObstacleSpawnPoint->GetScaledBoxExtent());
	int RandomNumber = FMath::RandRange(0, 1);
	switch (RandomNumber)
	{
	case 0:
		UE_LOG(LogTemp, Warning, TEXT("ObstacleMoved"));
		TileObstacle = GetWorld()->SpawnActor<AObstacle>(Obstacle[0], FTransform(ObstacleLocation));
		TileObstacle->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
		break;

	case 1:
		UE_LOG(LogTemp, Warning, TEXT("PickUp"));
		TilePickUp = GetWorld()->SpawnActor<APickUp>(PickUp[0], FTransform(ObstacleLocation));
		TilePickUp->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
		break;
	}
	
}

// Called every frame
void ARunTile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

