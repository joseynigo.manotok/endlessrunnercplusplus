// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class CENDLESSRUNNER_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

public:
	//Constructor
	ARunCharacterController();

	virtual void SetupInputComponent();

	virtual void CallMoveRight(float scale);

	void Disable();

protected:
		virtual void BeginPlay() override;

		UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ARunCharacter* PlayerCharacter;
};
