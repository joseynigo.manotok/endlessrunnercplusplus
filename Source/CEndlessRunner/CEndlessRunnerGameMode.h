// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CEndlessRunnerGameMode.generated.h"

UCLASS(minimalapi)
class ACEndlessRunnerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACEndlessRunnerGameMode();
};



