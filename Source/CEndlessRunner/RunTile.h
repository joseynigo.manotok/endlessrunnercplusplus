// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RunTile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FRunCharacterOverlap, class ARunTile*, Tile);

UCLASS()
class CENDLESSRUNNER_API ARunTile : public AActor
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ARunTile();

	FTimerHandle TileTimer;

	FVector ObstacleLocation;

	UPROPERTY(EditAnywhere)
		class AObstacle* TileObstacle;

	UPROPERTY(EditAnywhere)
		class APickUp* TilePickUp;

	UPROPERTY(BlueprintAssignable, Category = "Event Dispatcher")
		FRunCharacterOverlap OnCharacterOverlap;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UArrowComponent* AttachPoint;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UBoxComponent* ExitTrigger;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UBoxComponent* ObstacleSpawnPoint;

	UFUNCTION()
		void ExitOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void SpawnObstacle();

	UFUNCTION()
		void DestroyActorOnTimer();


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:

	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<class AObstacle>> Obstacle;

	UPROPERTY(EditAnywhere)
		TArray<TSubclassOf<class APickUp>> PickUp;
};
